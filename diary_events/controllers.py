from flask import request, redirect, url_for, render_template, Blueprint, flash
from flask_login import login_required, current_user

from accounts.models import User
from database import db
from diary_events.models import ToDos, ToDoUsers
from diary_events.services import get_events, delete_events, save_event, create_dependence_todo_user_events, \
    prepare_todo_data, save_todo, user_add_todo, create_dependence_user_events, complete_todo_event

diary_blueprint = Blueprint('diary', __name__, template_folder='templates')


@diary_blueprint.route('/', methods=['GET'])
@login_required
def index():
    return render_template('diary_events/diary.html')


@diary_blueprint.route('/<int:month>/<int:day>/<int:year>', methods=['GET', 'POST'])
@login_required
def events(month, day, year):
    date = '{}/{}/{}'.format(month, day, year)
    event_list = get_events(current_user.id, date)
    if request.method == 'POST':
        if request.form['btn'] == 'Complete':
            print(2)
            event_id = request.form['list']
            delete_events(event_id)
            return redirect(url_for('diary.events', month=month, day=day, year=year))
        else:
            event = save_event(request)
            event.date_field = '{}/{}/{}'.format(month, day, year)
            db.session.add(event)
            db.session.commit()
            events_user = create_dependence_user_events(event.id, current_user.id, )
            db.session.add(events_user)
            db.session.commit()
            return redirect(url_for('diary.events', month=month, day=day, year=year))
    return render_template('diary_events/events.html', events=event_list)


@diary_blueprint.route('/todo/<hash_id>', methods=['GET', 'POST'])
@login_required
def todo(hash_id):
    todo_d = ToDos.query.filter_by(hash=hash_id).first()
    if request.method == 'POST':
        if request.form['btn'] == 'add':
            user = User.query.filter_by(email=request.form['mail']).first()
            try:
                user_add_todo(hash_id, user)
                create_dependence_todo_user_events(request.form['event'], user.id, todo_d.id,
                                                   status=request.form['status'])
            except Exception as e:
                flash(str(e) or 'Unknown error')
            else:
                return redirect(url_for('diary.todo', hash_id=hash_id))
        else:
            complete_todo_event(request.form['btn'])
            return redirect(url_for('diary.todo', hash_id=hash_id))
    name = User.query.filter_by(id=current_user.id).first().username
    current_event = ToDoUsers.query.filter_by(todo_id=todo_d.id, user_id=current_user.id).first()
    role = current_event.role if current_event else 'Performer'
    todos = prepare_todo_data(todo_d.id)
    return render_template('diary_events/todo.html', todos=todos, current_user_role=role, name=name)


@diary_blueprint.route('/manage', methods=['GET', 'POST'])
@login_required
def manage_todo():
    if request.method == 'POST':
        if request.form['btn'] == 'search':
            todo_hash = request.form['search_hash']
            todo_d = ToDos.query.filter_by(hash=todo_hash).first()
            if todo_d:
                return redirect(url_for('diary.todo', hash_id=todo_hash))
            else:
                flash('Incorrect hash')
        else:
            try:
                hash_todo = save_todo(request, current_user.id)
            except Exception as e:
                flash(str(e) or 'Unknown error')
            else:
                return redirect(url_for('diary.todo', hash_id=hash_todo))
    return render_template('diary_events/manage.html')
