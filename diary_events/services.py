from datetime import datetime

from sqlalchemy.sql.functions import current_date

from werkzeug.security import generate_password_hash

from accounts.models import User
from accounts.services import NotFoundError, FieldIsNotUnique
from database import db
from diary_events.models import Events, ToDos, ToDoUsers, ToDoEvents, EventsUser


def get_events(user_id, date):
    events = None
    with db.engine.connect() as conn:
        sql = "SELECT e.id, e.name, e.start_date, e.end_date, e.classes FROM Events as e " \
              "LEFT JOIN EventsUser as eu on e.id = eu.event_id " \
              "LEFT JOIN users as u on u.id = eu.user_id " \
              "WHERE u.id = :user_id AND e.date_field = :date"

        events = [event for event in conn.execute(sql, user_id, date)]
    return events


def delete_events(id):
    with db.engine.connect() as conn:
        sql = "DELETE FROM Events WHERE id = :id;"
        conn.execute(sql, id)
        sql = "DELETE FROM EventsUser WHERE event_id = :id"
        conn.execute(sql, id)


def save_event(request):
    event = Events()
    event.name = request.form['name']
    if request.form['start_date']:
        event.start_date = datetime.strptime(request.form['start_date'], "%m/%d/%Y").date()
    else:
        event.start_date = current_date()
    if request.form['end_date']:
        event.end_date = datetime.strptime(request.form['end_date'], "%m/%d/%Y").date()
    else:
        event.end_date = event.start_date
    event.classes = request.form['classes']
    return event


def save_todo(request, id):
    todo = ToDos(name=request.form['name'], hash=request.form['hash'],
                 password=generate_password_hash(request.form['password']))
    if not ToDos.query.filter_by(hash=todo.hash).first():
        db.session.add(todo)
        db.session.commit()
        todo_user = ToDoUsers(todo_id=todo.id, user_id=id, role='Creator')
        db.session.add(todo_user)
        db.session.commit()
    elif not ToDos.query.filter_by(name=todo.name).first():
        raise FieldIsNotUnique('Not unique name')
    else:
        raise FieldIsNotUnique('Not unique hash')
    return todo.hash


def get_current_todos(user_id):
    todos = [todo for todo in ToDoUsers.query.filter_by(user_id=user_id)]
    return todos


def user_add_todo(hash_t, user: User):
    if user:
        todo = ToDos.query.filter_by(hash=hash_t).first()
        if not ToDoUsers.query.filter_by(user_id=user.id, todo_id=todo.id).first():
            todo_user = ToDoUsers(todo_id=todo.id, user_id=user.id)
            db.session.add(todo_user)
            db.session.commit()
    else:
        raise NotFoundError('Incorrect email')


def complete_todo_event(event_id):
    with db.engine.connect() as conn:
        sql = "DELETE FROM ToDoEvents WHERE id = :event_id"
        conn.execute(sql, event_id)


def prepare_todo_data(todo_id):
    todo_data = []
    for event in ToDoEvents.query.filter_by(todo_id=todo_id):
        todo_data.append({'event': event.task, 'status': event.status, 'id': event.id,
                          'username': User.query.filter_by(id=event.user_id).first().username})
    return todo_data


def create_dependence_todo_user_events(event, user_id, todo_id, status='ToDo'):
    events_user = ToDoEvents()
    events_user.todo_id = todo_id
    events_user.user_id = user_id
    events_user.task = event
    events_user.status = status
    db.session.add(events_user)
    db.session.commit()


def create_dependence_user_events(event_id, user_id):
    events_user = EventsUser()
    events_user.user_id = user_id
    events_user.event_id = event_id
    return events_user
