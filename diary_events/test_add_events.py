from datetime import datetime

import pytest
from flask import url_for
from app import db
from diary_events.models import Events, EventsUser


def create_events(event, user_id):
    event_to_db = Events()
    event_to_db.name = event[0]
    event_to_db.date_field = event[1]
    event_to_db.start_date = datetime.strptime(event[2], "%m/%d/%Y").date()
    event_to_db.end_date = datetime.strptime(event[3], "%m/%d/%Y").date()
    event_to_db.classes = event[4]
    db.session.add(event_to_db)
    db.session.commit()
    events = EventsUser(event_id=event_to_db.id, user_id=user_id)
    db.session.add(events)
    db.session.commit()


def test_page_with_calendar(logged_in_client):
    res = logged_in_client.get(url_for('diary.index'))
    assert res.status_code == 200


@pytest.mark.parametrize('event', [
    (['Cleaning', '10/10/2021', '10/10/2021', 'First'])
])
def test_add_event_on_date(logged_in_client, event):
    name, start_date, end_date, classes = event
    res = logged_in_client.post(url_for('diary.events', month=10, day=10, year=2021),
                                data=dict(name=name, start_date=start_date, end_date=end_date, classes=classes, btn=''),
                                follow_redirects=True)
    assert res.status_code == 200
    assert Events.query.filter_by(name=name).count() == 1


@pytest.mark.parametrize('event, user_id', [
    (['Cleaning', '10/10/2021', '10/10/2021', '10/10/2021', 'First'], 1)
])
def test_view_event(logged_in_client, event, user_id):
    create_events(event, user_id)
    res = logged_in_client.get(url_for('diary.events', month=10, day=10, year=2021))
    assert res.status_code == 200
    assert bytes(event[0], 'utf-8') in res.data
