from sqlalchemy.sql.functions import current_date

from database import db


class Events(db.Model):
    __tablename__ = 'Events'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=False, nullable=False)
    date_field = db.Column(db.String, unique=False, index=True)
    start_date = db.Column(db.Date, default=current_date, nullable=False)
    end_date = db.Column(db.Date, default=current_date, nullable=False)
    classes = db.Column(db.String, nullable=False)


class EventsUser(db.Model):
    __tablename__ = 'EventsUser'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    event_id = db.Column(db.Integer, db.ForeignKey('Events.id'), nullable=False)
    is_completed = db.Column(db.Boolean, default=False)
    

class ToDos(db.Model):
    __tablename__ = 'ToDos'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False, index=True)
    password = db.Column(db.String, nullable=False)
    hash = db.Column(db.String, nullable=False, unique=True)


class ToDoUsers(db.Model):
    __tablename__ = 'ToDoUsers'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    todo_id = db.Column(db.Integer, db.ForeignKey('ToDos.id'), nullable=False)
    role = db.Column(db.String, nullable=False, default='Performer')


class ToDoEvents(db.Model):
    __tablename__ = 'ToDoEvents'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    todo_id = db.Column(db.Integer, db.ForeignKey('ToDos.id'), nullable=False)
    task = db.Column(db.String, nullable=False)
    status = db.Column(db.String, nullable=False, default='ToDo')
