from datetime import date

from flask_wtf import FlaskForm
from wtforms import StringField, DateField, SubmitField
from wtforms.validators import DataRequired


class EventForm(FlaskForm):
    name = StringField("Event name", validators=[DataRequired()])
    start_date = DateField("Start date", default=date.today())
    end_date = DateField("End date")
    classes = StringField("Event class")
    submit = SubmitField("Delete")

    def validate_on_submit(self):
        result = super(EventForm, self).validate()
        if self.start_date.data > self.end_date.data if self.end_date.data else True:
            return False
        else:
            return result
