import pytest
from flask import url_for

from app import db
from diary_events.models import ToDos, ToDoUsers


@pytest.fixture
def create_todo():
    todo = ToDos(name='New', password='1234', hash='1234')
    todo_user = ToDoUsers(todo_id=1, user_id=1, role='Creator')
    if ToDos.query.filter_by(name=todo.name).first():
        pass
    else:
        db.session.add(todo_user)
        db.session.commit()
        db.session.add(todo)
        db.session.commit()


@pytest.mark.parametrize('name, password, hash_t', [
    ('New', '1234', '12344'),
    ('New_1', '1234', '12342'),
    ('N_1', '1234cxz', '12')
])
def test_create_and_search_todo(logged_in_client, name, password, hash_t):
    res = logged_in_client.post(url_for('diary.manage_todo'), data=dict(name=name, hash=hash_t,
                                                                        password=password, btn='create'),
                                follow_redirects=True)
    assert res.status_code == 200
    assert res.request.path == url_for('diary.todo', hash_id=hash_t)


@pytest.mark.parametrize('name, password, hash_t, flash', [
    ('New1', '1234', '12344', "Our ToDo"),
    ('New', '12341', '12342', 'flash'),
    ('N_1', '1234', '1234', 'flash')
])
def test_create_and_search_todo(logged_in_client, name, password, hash_t, flash, create_todo):
    res = logged_in_client.post(url_for('diary.manage_todo'), data=dict(name=name, hash=hash_t,
                                                                        password=password, btn='create'),
                                follow_redirects=True)
    assert res.status_code == 200
    assert bytes(flash, 'utf-8') in res.data


@pytest.mark.parametrize('hash_t, flash, path', [
    ('1234', 'Our ToDo', '/diary/todo/1234'),
    ('12344qdw', 'flash', '/diary/manage')
])
def test_search_func(logged_in_client, hash_t, flash, create_todo, path):
    res = logged_in_client.post(url_for('diary.manage_todo'), data=dict(search_hash=hash_t, btn='search'),
                                follow_redirects=True)

    assert res.status_code == 200
    assert bytes(flash, 'utf-8') in res.data
    assert path == res.request.path


def test_add_todo_func(logged_in_client, create_todo):
    res = logged_in_client.post(url_for('diary.todo', hash_id='12344'), data=dict(event='Event', btn='add',
                                                                                  mail='mops@mail.ru', status='Todo'))
    assert res.status_code == 200
    assert b'Complete' in res.data


def test_complete_func(logged_in_client, create_todo):
    res = logged_in_client.post(url_for('diary.todo', hash_id='12344'), data=dict(btn='1'), follow_redirects=True)

    assert res.status_code == 200
    assert b'Event' not in res.data
