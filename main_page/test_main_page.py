from flask import url_for


def test_main_page_without_login_user(client):
    res = client.get(url_for('main_page.main_page'))
    assert res.status_code == 200
    assert b'login' in res.data


def test_main_page_with_login_user(logged_in_client):
    res = logged_in_client.get(url_for('main_page.main_page'))
    assert res.status_code == 200
    assert b'Log In' not in res.data


def test_error_handler(logged_in_client):
    res = logged_in_client.get(url_for('accounts.other_user_info', user_id=253))

    assert res.status_code == 404
    assert b'Page not found'