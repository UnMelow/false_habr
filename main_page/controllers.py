from flask import Blueprint, render_template

main_page_blueprint = Blueprint('main_page', __name__, template_folder='templates')


@main_page_blueprint.route('/', methods=('GET',))
def main_page():
    return render_template('main_page/index.html')
