import pytest
from flask import url_for


@pytest.mark.parametrize('name, email, password, repeat_pas, flash_mes', [
    ('Mops', 'afwfaw@mail.ru', '1234', '1234', 'This username is already registered'),
    ('Mops1a', 'qwear@mail.ru', '1234', '1232', 'Password miss match')
])
def test_incorrect_registration_form(client, name, email, password, repeat_pas, flash_mes):
    res = client.post(url_for('accounts.register'), data=dict(username=name, email=email, password=password,
                                                              repeat_pas=repeat_pas), follow_redirects=True)
    assert bytes(flash_mes, 'utf-8') in res.data
