import pytest
from flask import url_for


def test_current_user_info_for_logged_in_should_be_ok(logged_in_new_client, logged_in_client):
    res = logged_in_new_client.get(url_for('accounts.user_info'))
    assert res.status_code == 200
    assert b'Mops' in res.data

    res = logged_in_client.get(url_for('accounts.logout'))
    assert res.status_code == 200
    assert b'Log In' in res.data


def test_current_user_info_for_logged_out_should_be_ok(client):
    res = client.get(url_for('accounts.user_info'))
    assert res.status_code == 401
    assert b'Mops' not in res.data


@pytest.mark.parametrize('name, email, password', [
    ('', None, 'mops'),
    ('mops', None, 'mops'),
    ('Mops1', 'mops1@mail.ru', 'mops')
]
)
def test_edit_user_info_with_password(logged_in_client, new_user, name, email, password):
    res = logged_in_client.post(url_for('accounts.edit_profile'), data=dict(name=name, email=email, password=password),
                                follow_redirects=True)

    assert res.status_code == 200
    assert res.request.path == url_for('accounts.user_info')
    assert bytes(name, 'utf-8') if name else bytes(new_user.username, 'utf-8') in res.data
    assert bytes(email, 'utf-8') if email else bytes(new_user.email, 'utf-8') in res.data


@pytest.mark.parametrize('name, email, password', [
    ('', None, 'mops1'),
    ('mops', None, 'mops1'),
    ('Mops1', 'mops1@mail.ru', 'mops1')
]
)
def test_edit_user_info_with_incorrect_password(logged_in_client, new_user, name, email, password):
    res = logged_in_client.post(url_for('accounts.edit_profile'), data=dict(name=name, email=email, password=password),
                                follow_redirects=True)

    assert res.status_code == 200
    assert b'Incorrect password' in res.data
