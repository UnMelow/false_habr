from flask_login import current_user
from werkzeug.security import check_password_hash
from accounts.models import User
from database import db


class NotFoundError(Exception):
    pass


class FieldIsNotUnique(Exception):
    pass


class IncorrectPasswordError(Exception):
    pass


def edit_profile_impl(form, user_id):
    user = find_user_by_id(user_id)
    email = user.email if not form.email.data else form.email.data
    username = user.username if not form.username.data else form.username.data
    password = form.password.data
    if username != user.username and User.query.filter_by(username=username).first():
        raise FieldIsNotUnique('This username is already registered')
    if email != user.email and User.query.filter_by(email=email).first():
        raise FieldIsNotUnique('This email is already registered')
    user_data = {'username': username, 'email': email, 'id': current_user.id}
    if check_password_hash(current_user.hashed_password, password):
        with db.engine.connect() as conn:
            sql = "update users set email=:email, username=:username where id=:id"
            conn.execute(sql, **user_data)
    else:
        raise IncorrectPasswordError('Incorrect password')


def find_by_username_and_password(username: str, password: str):
    user = User.query.filter_by(username=username).first()
    if not user:
        raise NotFoundError('Incorrect username')
    if not check_password_hash(user.hashed_password, password):
        raise IncorrectPasswordError('Incorrect password')
    return user


def register_user(name:str, email:str, password, rep_password):
    if User.query.filter_by(username=name).first():
        raise FieldIsNotUnique('This username is already registered')
    if User.query.filter_by(email=email).first():
        raise FieldIsNotUnique('This email is already registered')
    if password == rep_password:
        user = User(username=name, email=email)
        user.set_password(password)
        return user
    else:
        raise IncorrectPasswordError('Password miss match')


def find_user_by_id(user_id: int):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        raise NotFoundError()
    return user
