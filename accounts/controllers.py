from flask import request, redirect, url_for, flash, render_template, Blueprint, abort
from flask_login import login_user, login_required, logout_user, current_user

from accounts.forms import RegistrationForm, EditProfile, LoginForm
from accounts.services import find_by_username_and_password, register_user, find_user_by_id, \
    NotFoundError, edit_profile_impl
from database import db

accounts_blueprint = Blueprint('accounts', __name__, template_folder='templates')


@accounts_blueprint.route('/login', methods=('GET', 'POST'))
def login():
    form = LoginForm()
    if request.method == 'POST' and form.validate():
        username = request.form['username']
        password = request.form['password']
        remember_me = len(request.form.getlist('remember_me')) > 0
        try:
            user = find_by_username_and_password(username, password)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            return redirect(url_for('accounts.login'))
        else:
            login_user(user, remember=remember_me)
            return redirect(url_for('main_page.main_page'))
    return render_template('accounts/login.html', form=form)


@accounts_blueprint.route('/register', methods=('GET', 'POST'))
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if request.method == 'POST':
        username = form.data['username']
        email = form.data['email']
        password = form.data['password']
        rep_password = form.data['repeat_pas']
        try:
            user = register_user(username, email, password, rep_password)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            return redirect(url_for('accounts.register'))
        else:
            db.session.add(user)
            db.session.commit()
            flash('Congratulations, you are now a registered user!')
            return redirect(url_for('accounts.login'))
    return render_template('accounts/registration.html', title='Register', form=form)


@accounts_blueprint.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return render_template('main_page/index.html')


@accounts_blueprint.route("/user", methods=["GET"])
@login_required
def user_info():
    return render_template('accounts/profile.html', user=current_user)


@accounts_blueprint.route("/user/<int:user_id>", methods=["GET", 'POST'])
@login_required
def other_user_info(user_id):
    try:
        user = find_user_by_id(user_id)
        return render_template('accounts/profile.html', user=user)
    except NotFoundError:
        abort(404)


@accounts_blueprint.route("/edit_user", methods=('GET', 'POST'))
@login_required
def edit_profile():
    form = EditProfile()
    if request.method == 'POST':
        try:
            edit_profile_impl(form, current_user.id)
        except Exception as e:
            flash(str(e))
        else:
            return redirect(url_for('accounts.user_info'))
    return render_template('accounts/edit_profile.html', user=current_user, form=form)
