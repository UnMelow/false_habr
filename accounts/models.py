from flask_login import UserMixin
from werkzeug.security import generate_password_hash

from database import db


class User(UserMixin, db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    email = db.Column(db.String(100), index=False, nullable=False, unique=True)
    hashed_password = db.Column(db.String(80), unique=False, nullable=False)
    events = db.relationship('EventsUser', backref="users", lazy='dynamic')
    todos = db.relationship('ToDoUsers', backref='users', lazy='dynamic')

    def set_password(self, password):
        self.hashed_password = generate_password_hash(password)

    def __repr__(self):
        return f"<User {self.username}>"
