from flask_login import LoginManager

from accounts.models import User


login_manager = LoginManager()


@login_manager.user_loader
def load_user(user_id):
    if user_id is not None:
        return User.query.get(int(user_id))
    return None
