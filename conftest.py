import pytest
from flask import url_for
from werkzeug.security import generate_password_hash

import app as app_mod
from accounts.models import User
from diary_events.models import ToDos, EventsUser, Events, ToDoUsers


@pytest.fixture
def app():
    app_mod.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app_mod.app.config['WTF_CSRF_ENABLED'] = False
    with app_mod.app.app_context():
        app_mod.db.create_all()
    yield app_mod.app


@pytest.fixture(scope='module')
def new_user(username='Mops', email='mops@mail.ru', password='mops'):
    user = User(username=username, email=email)
    user.set_password(password)
    return user


@pytest.fixture
def logged_in_new_client(client, new_user):
    app_mod.db.session.add(new_user)
    app_mod.db.session.commit()
    client.post(url_for('accounts.login'), data=dict(username=new_user.username, password='mops'))
    yield client


@pytest.fixture
def logged_in_client(client):
    client.post(url_for('accounts.login'), data=dict(username='Mops', password='mops'))
    yield client



