# Your_calendar (покрытие тестами: 90%)



**Инструкция по запуску приложения:**
* Склонировать репозиторий к себе на рабочий стол, нажав на кнопку clone наверху и выбрав `--clone with HTTPS`
  ![image1.png](image1.png)
  
* Потом:
  `cd false_habr` 
  `python -m venv venv` 
  `venv\Scripts\activate.bat` 
  `pip install -r requirements.txt` 
  `python init_database.py`
* Чтобы запустить приложение, необходимо оставаться в папке project и набрать команду:
  `python -m flask run`  
* Далее нужно перейти по указанной ссылке:
* Можно пользоваться!
