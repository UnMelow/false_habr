from werkzeug.security import generate_password_hash

from accounts.models import User
from diary_events.models import Events, ToDoUsers, ToDos, EventsUser
from app import db, app


def main():
    with app.app_context():
        db.create_all()
        db.session.commit()


if __name__ == '__main__':
    main()